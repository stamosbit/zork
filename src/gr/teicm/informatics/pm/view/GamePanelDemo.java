/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.view;

import gr.teicm.informatics.pm.game.AbstractGame;
import gr.teicm.informatics.pm.game.TextBasedAdventureGame;
import gr.teicm.informatics.pm.game.command.AbstractCommand;
import gr.teicm.informatics.pm.game.util.Parser;
import gr.teicm.informatics.pm.view.GameForm;

import java.util.Scanner;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author Stamos
 */
public class GamePanelDemo extends JPanel implements ActionListener {
    
    private static final String GAME_NAME = "The Pirate Day";
    
    private static final GameForm gameForm = new GameForm(GAME_NAME);
    
    private static AbstractCommand command;
    
    private static AbstractGame game;
    
    private static final String NEW_LINE = "\n";
    
    private String inputText;
    
    private String gameOutcome;
    
    /**
     * The client's method.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        game = new TextBasedAdventureGame(GAME_NAME);
        
        gameForm.add(new GamePanelDemo());
        
        gameForm.pack();
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                
            }
        });
    }
    
    private JTextField textField;
    
    private JTextArea textArea;
    
    /**
     * Initializes a newly created <tt>GameForm</tt> object.
     */
    private GamePanelDemo() {
        super(new GridBagLayout());
        
        prepareSecondaryComponents();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        inputText = textField.getText().trim();
        
        try {
            parseNewInput(inputText);
            
            assembleGameAction();
            
            game.run();
            
            gameOutcome = game.getOutcome();
            
            textArea.append(
                inputText + NEW_LINE +
                gameOutcome + NEW_LINE +
                NEW_LINE
            );
        } catch (IllegalArgumentException ex) {
            String message = ex.getMessage();
            
            textArea.append(
                inputText + NEW_LINE +
                message + NEW_LINE +
                NEW_LINE
            );
        }
        
        textField.setText("");

        // Make sure the new text is visible, even if there
        // was a selection in the text area.
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
    
    private void prepareSecondaryComponents() {
        textField = new JTextField(20);
        
        textField.addActionListener(this);
        
        textArea = new JTextArea(5, 20);
        
        textArea.setEditable(false);
        
        JScrollPane scrollPane = new JScrollPane(textArea);
        
        GridBagConstraints c = new GridBagConstraints();
        
        Integer spacing = 10;
        
        c.insets = new Insets(spacing, spacing, spacing, spacing);
        
        c.gridwidth = GridBagConstraints.REMAINDER;
        
        c.fill = GridBagConstraints.HORIZONTAL;
        
        add(textField, c);
        
        c.fill = GridBagConstraints.BOTH;
        
        c.weightx = 1.0;
        
        c.weighty = 1.0;
        
        add(scrollPane, c);
    }
    
    private static void parseNewInput(String trimmedText) {
        Scanner scanner = new Scanner(trimmedText);
        
        Parser parser = new Parser(scanner);
        
        command = parser.getCommand();
    }
    
    private static void assembleGameAction() {
        command.setGame(game);
        
        command.executeCommand();
        
        game = command.getGame();
        
        command = null;
    }
    
}
