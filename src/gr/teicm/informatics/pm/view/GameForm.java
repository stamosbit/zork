/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.view;

import javax.swing.JFrame;

/**
 *
 * @author Stamos
 */
public class GameForm extends JFrame {
    
    public GameForm(String title) {
        super(title);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        setVisible(true);
    }
    
}
