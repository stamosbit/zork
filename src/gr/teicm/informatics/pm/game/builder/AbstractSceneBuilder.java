/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.scene.Scene;

/**
 *
 * @author Stamos
 */
abstract class AbstractSceneBuilder extends AbstractModelBuilder {
    
    protected final Scene scene;
    
    protected final String sceneName;
    
    protected ActorDirector actorDirector;
    
    protected AbstractSceneBuilder(Scene scene) {
        super();
        
        this.scene = scene;
        
        sceneName = this.scene.getName();
        
        actorDirector = new ActorDirector();
    }
    
    public void setActorBuilder(AbstractActorBuilder actorBuilder) {
        this.actorDirector.setActorBuilder(actorBuilder);
    }
    
    public abstract void buildActorContainer();
    
    public Scene getScene() {
        return scene;
    }
    
}
