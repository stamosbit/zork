/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.scene.BigForest;

/**
 *
 * @author Stamos
 */
class BigForestBuilder extends AbstractSceneBuilder {
    
    public BigForestBuilder() {
        super(new BigForest());
    }
    
    @Override
    public void buildActorContainer() {
        
    }
    
    @Override
    public void buildItemContainer() {
        
    }
}
