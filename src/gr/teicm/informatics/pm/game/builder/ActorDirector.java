/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;

/**
 *
 * @author Stamos
 */
class ActorDirector {
    
    private AbstractActorBuilder actorBuilder;
    
    public ActorDirector() {
        this.actorBuilder = null;
    }
    
    public void setActorBuilder(AbstractActorBuilder actorBuilder) {
        this.actorBuilder = actorBuilder;
    }
    
    public void prepareActor() {
        actorBuilder.buildItemContainer();
        
        actorBuilder.buildWeapons();
    }
    
    public AbstractActor getActor() {
        return actorBuilder.getActor();
    }
    
}
