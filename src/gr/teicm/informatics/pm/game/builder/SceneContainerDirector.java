/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.container.ISceneContainer;

/**
 *
 * @author Stamos
 */
public class SceneContainerDirector {
    
    private final SceneContainerBuilder sceneContainerBuilder;
    
    public SceneContainerDirector() {
        sceneContainerBuilder = new SceneContainerBuilder();
    }
    
    public void prepareContainer() {
        sceneContainerBuilder.buildContainer();
    }
    
    public ISceneContainer getContainer() {
        return sceneContainerBuilder.getContainer();
    }
    
}
