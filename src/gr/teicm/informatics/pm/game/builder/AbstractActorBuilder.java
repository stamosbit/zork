/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;

/**
 *
 * @author Stamos
 */
abstract class AbstractActorBuilder extends AbstractModelBuilder {
    
    protected AbstractActor actor;
    
    protected AbstractActorBuilder(AbstractActor actor) {
        super();
        this.actor = actor;
    }
    
    public abstract void buildWeapons();
    
    public AbstractActor getActor() {
        return actor;
    }
    
}
