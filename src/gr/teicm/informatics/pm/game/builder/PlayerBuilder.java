/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.actor.Player;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.item.weapon.AbstractWeapon;

/**
 *
 * @author Stamos
 */
class PlayerBuilder extends AbstractActorBuilder {
    
    public PlayerBuilder() {
        super(new Player());
    }
    
    @Override
    public void buildItemContainer() {
        AbstractItem potion = itemFactory.getInstance(POTION_NAME);
        
        AbstractItem book = itemFactory.getInstance(BOOK_NAME);
        
        actor.putItem(potion);
        
        actor.putItem(book);
    }
    
    @Override
    public void buildWeapons() {
        AbstractWeapon knife =
                (AbstractWeapon) itemFactory.getInstance(KNIFE_NAME);
        
        AbstractWeapon pistol =
                (AbstractWeapon) itemFactory.getInstance(PISTOL_NAME);
        
        actor.setKnife(knife);
        
        actor.setPistol(pistol);
    }
    
}
