/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.scene.Dungeon;

/**
 *
 * @author Stamos
 */
class DungeonBuilder extends AbstractSceneBuilder {
    
    public DungeonBuilder() {
        super(new Dungeon());
    }
    
    @Override
    public void buildActorContainer() {
        actorDirector.setActorBuilder(new PirateBuilder());
        
        actorDirector.prepareActor();
        
        AbstractActor pirate = actorDirector.getActor();
        
        pirate.setCurrentSceneName(sceneName);
        
        scene.putActor(pirate);
    }
    
    @Override
    public void buildItemContainer() {
        
    }
    
}
