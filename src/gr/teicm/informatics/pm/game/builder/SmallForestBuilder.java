/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.scene.SmallForest;

/**
 *
 * @author Stamos
 */
class SmallForestBuilder extends AbstractSceneBuilder {
    
    public SmallForestBuilder() {
        super(new SmallForest());
    }
    
    @Override
    public void buildActorContainer() {
        
    }
    
    @Override
    public void buildItemContainer() {
        
    }
    
}
