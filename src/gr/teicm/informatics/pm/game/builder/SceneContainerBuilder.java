/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.container.ISceneContainer;
import gr.teicm.informatics.pm.game.model.container.SceneMap;
import gr.teicm.informatics.pm.game.model.scene.Scene;

/**
 *
 * @author Stamos
 */
class SceneContainerBuilder implements
        IContainerBuilder<ISceneContainer> {
    
    private final ISceneContainer sceneContainer;
    
    private final SceneDirector sceneDirector;
    
    public SceneContainerBuilder() {
        sceneContainer = new SceneMap();
        
        sceneDirector = new SceneDirector();
    }
    
    @Override
    public void buildContainer() {
        sceneDirector.setSceneBuilder(new HouseBuilder());
        
        sceneDirector.prepareScene();
        
        Scene house = sceneDirector.getScene();
        
        sceneDirector.setSceneBuilder(new SmallForestBuilder());
        
        sceneDirector.prepareScene();
        
        Scene smallForest = sceneDirector.getScene();
        
        sceneDirector.setSceneBuilder(new FarmBuilder());
        
        sceneDirector.prepareScene();
        
        Scene farm = sceneDirector.getScene();
        
        sceneDirector.setSceneBuilder(new BigForestBuilder());
        
        sceneDirector.prepareScene();
        
        Scene bigForest = sceneDirector.getScene();
        
        sceneDirector.setSceneBuilder(new DungeonBuilder());
        
        sceneDirector.prepareScene();
        
        Scene dungeon = sceneDirector.getScene();
        
        sceneDirector.setSceneBuilder(new CoastBuilder());
        
        sceneDirector.prepareScene();
        
        Scene coast = sceneDirector.getScene();
        
        house = buildSceneRelationships(
                house,
                smallForest.getName(),
                "",
                farm.getName(),
                bigForest.getName()
        );
        
        smallForest = buildSceneRelationships(
                smallForest,
                dungeon.getName(),
                house.getName(),
                "",
                ""
        );
        
        farm = buildSceneRelationships(
                farm,
                "",
                "",
                "",
                house.getName()
        );
        
        bigForest = buildSceneRelationships(
                bigForest,
                "",
                "",
                house.getName(),
                ""
        );
        
        dungeon = buildSceneRelationships(
                dungeon,
                "",
                smallForest.getName(),
                "",
                coast.getName()
        );
        
        coast = buildSceneRelationships(
                coast,
                "",
                "",
                dungeon.getName(),
                ""
        );
        
        sceneContainer.putScene(house);
        
        sceneContainer.putScene(smallForest);
        
        sceneContainer.putScene(farm);
        
        sceneContainer.putScene(bigForest);
        
        sceneContainer.putScene(dungeon);
        
        sceneContainer.putScene(coast);
    }
    
    private Scene buildSceneRelationships(
            Scene scene,
            String northSceneName,
            String southSceneName,
            String westSceneName,
            String eastSceneName
    ) {
        scene.setNorthSceneName(northSceneName);
        
        scene.setSouthSceneName(southSceneName);
        
        scene.setWestSceneName(westSceneName);
        
        scene.setEastSceneName(eastSceneName);
        
        return scene;
    }
    
    @Override
    public ISceneContainer getContainer() {
        return sceneContainer;
    }
    
}
