/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.scene.House;

/**
 *
 * @author Stamos
 */
class HouseBuilder extends AbstractSceneBuilder {
    
    public HouseBuilder() {
        super(new House());
    }

    @Override
    public void buildActorContainer() {
        actorDirector.setActorBuilder(new PlayerBuilder());
        
        actorDirector.prepareActor();
        
        AbstractActor player = actorDirector.getActor();
        
        player.setCurrentSceneName(sceneName);
        
        scene.putActor(player);
    }

    @Override
    public void buildItemContainer() {
        AbstractItem book = itemFactory.getInstance(BOOK_NAME);
        
        AbstractItem chest = itemFactory.getInstance(CHEST_NAME);
        
        scene.putItem(book);
        
        scene.putItem(chest);
    }
    
}
