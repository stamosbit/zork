/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.constant.ItemConstants;
import gr.teicm.informatics.pm.game.factory.ItemFactory;

/**
 *
 * @author Stamos
 */
abstract class AbstractModelBuilder {
    
    protected static final
            String POTION_NAME = ItemConstants.POTION.name().toLowerCase();
    
    protected static final
            String DRUG_NAME = ItemConstants.DRUG.name().toLowerCase();
    
    protected static final
            String KNIFE_NAME = ItemConstants.KNIFE.name().toLowerCase();
    
    protected static final
            String PISTOL_NAME = ItemConstants.PISTOL.name().toLowerCase();
    
    protected static final
            String BOOK_NAME = ItemConstants.BOOK.name().toLowerCase();
    
    protected static final
            String CHEST_NAME = ItemConstants.CHEST.name().toLowerCase();
    
    protected static final
            String DOOR_NAME = ItemConstants.DOOR.name().toLowerCase();
    
    protected final ItemFactory itemFactory;
    
    protected AbstractModelBuilder() {
        itemFactory = new ItemFactory();
    }
    
    public abstract void buildItemContainer();
    
}
