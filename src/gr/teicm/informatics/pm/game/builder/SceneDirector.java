/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

import gr.teicm.informatics.pm.game.model.scene.Scene;

/**
 *
 * @author Stamos
 */
class SceneDirector {
    
    private AbstractSceneBuilder sceneBuilder;
    
    public SceneDirector(AbstractSceneBuilder sceneBuilder) {
        this.sceneBuilder = sceneBuilder;
    }
    
    public SceneDirector() {
        this(null);
    }
    
    public void setSceneBuilder(AbstractSceneBuilder sceneBuilder) {
        this.sceneBuilder = sceneBuilder;
    }
    
    public void prepareScene() {
        sceneBuilder.buildItemContainer();
        
        sceneBuilder.buildActorContainer();
    }
    
    public Scene getScene() {
        return sceneBuilder.getScene();
    }
    
}
