/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.constant.ItemConstants;

/**
 *
 * @author Stamos
 */
public class OpenAction extends AbstractAction {
    
    /**
     * Initializes a newly created <tt>OpenAction</tt> object.
     * 
     * @param objective
     */
    public OpenAction(String objective) {
        super(OPEN_COMMAND, objective);
    }

    @Override
    public void performAction() {
        
    }
    
}
