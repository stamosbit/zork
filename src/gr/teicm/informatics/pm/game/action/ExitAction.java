/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

/**
 *
 * @author 4340
 */
public class ExitAction extends AbstractAction {
    
    /**
     * Initializes a newly created <tt>ExitAction</tt> object.
     */
    public ExitAction() {
        super(EXIT_COMMAND, NO_OBJECTIVE);
    }

    @Override
    public void performAction() {
        System.exit(0);
    }
    
}
