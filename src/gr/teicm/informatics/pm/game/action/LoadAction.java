/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.controller.Context;
import gr.teicm.informatics.pm.game.util.serialization.ContextDeserializer;
import gr.teicm.informatics.pm.game.util.serialization.IDeserializer;

/**
 *
 * @author Stamos
 */
public class LoadAction extends AbstractAction {
    
    public LoadAction() {
        super(LOAD_COMMAND, NO_OBJECTIVE);
    }
    
    @Override
    public void performAction() {
        IDeserializer<Context> deserializer = new ContextDeserializer();
        
        deserializer.deserialize();
        
        context = deserializer.getInstance();
    }
    
}
