/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.constant.DirectionConstants;

/**
 *
 * @author Stamos
 */
public class RunAction extends AbstractAction {
    
    /**
     * Initializes a newly created <tt>RunAction</tt> object.
     * 
     * @param objective
     */
    public RunAction(String objective) {
        super(RUN_COMMAND, objective);
    }

    @Override
    public void performAction() {
        
    }
    
}
