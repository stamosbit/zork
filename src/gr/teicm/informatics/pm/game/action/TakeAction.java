/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.constant.ItemConstants;

/**
 *
 * @author Stamos
 */
public class TakeAction extends AbstractAction {
    
    /**
     * Initializes a newly created <tt>TakeAction</tt> object.
     * 
     * @param objective
     */
    public TakeAction(String objective) {
        super(TAKE_COMMAND, objective);
    }
    
    @Override
    public void performAction() {
        
    }
    
}
