/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.util.serialization.ContextSerializer;
import gr.teicm.informatics.pm.game.util.serialization.ISerializer;

/**
 *
 * @author Stamos
 */
public class SaveAction extends AbstractAction {
    
    public SaveAction() {
        super(SAVE_COMMAND, NO_OBJECTIVE);
        
        
    }
    
    @Override
    public void performAction() {
        ISerializer serializer = new ContextSerializer(context);
        
        serializer.serialize();
    }
    
}
