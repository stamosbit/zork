/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.constant.DirectionConstants;

/**
 *
 * @author Stamos
 */
public class GoAction extends AbstractAction {
    
    /**
     * Initializes a newly created <tt>GoAction</tt> object.
     * 
     * @param objective
     */
    public GoAction(String objective) {
        super(GO_COMMAND, objective);
    }
    
    @Override
    public void performAction() {
        DirectionConstants direction = DirectionConstants.valueOf(objective.toUpperCase());
        
        context.movePlayer(direction);
        
        contextOutcome = context.getCurrentLocation();
        
        context.setCurrentOutcome(contextOutcome);
    }
    
}
