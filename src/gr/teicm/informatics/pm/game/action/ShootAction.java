/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.constant.ActorConstants;

/**
 *
 * @author Stamos
 */
public class ShootAction extends AbstractAction {
    
    /**
     * Initializes a newly created <tt>ShootAction</tt> object.
     * 
     * @param objective
     */
    public ShootAction(String objective) {
        super(SHOOT_COMMAND, objective);
    }
    
    @Override
    public void performAction() {
        
    }
    
}
