/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.OpenAction;

/**
 *
 * @author Stamos
 */
public class OpenCommand extends AbstractCommand {
    
    public OpenCommand(String objective) {
        super(OPEN_COMMAND, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        OpenAction action = new OpenAction(objective);
        
        game.setAction(action);
    }
    
}
