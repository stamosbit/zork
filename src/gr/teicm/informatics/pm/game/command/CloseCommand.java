/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.CloseAction;

/**
 *
 * @author Stamos
 */
public class CloseCommand extends AbstractCommand {
    
    public CloseCommand(String objective) {
        super(CLOSE_COMMAND, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        CloseAction action = new CloseAction(objective);
        
        game.setAction(action);
    }
    
}
