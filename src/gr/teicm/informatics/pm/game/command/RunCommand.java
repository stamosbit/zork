/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.RunAction;

/**
 *
 * @author Stamos
 */
public class RunCommand extends AbstractCommand {
    
    public RunCommand(String objective) {
        super(RUN_COMMAND, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        RunAction action = new RunAction(objective);
        
        game.setAction(action);
    }
    
}
