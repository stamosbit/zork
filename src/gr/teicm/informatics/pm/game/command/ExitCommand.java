/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.action.ExitAction;

/**
 *
 * @author Stamos
 */
public class ExitCommand extends AbstractCommand {

    public ExitCommand() {
        super(EXIT_COMMAND, NO_OBJECTIVE);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void executeCommand() {
        AbstractAction action = new ExitAction();
        
        game.setAction(action);
    }
    
}
