/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.action.LoadAction;

/**
 *
 * @author Stamos
 */
public class LoadCommand extends AbstractCommand {
    
    public LoadCommand() {
        super(LOAD_COMMAND, NO_OBJECTIVE);
    }
    
    @Override
    public void executeCommand() {
        AbstractAction action = new LoadAction();
        
        game.setAction(action);
    }
    
}
