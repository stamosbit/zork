/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.AbstractGame;
import gr.teicm.informatics.pm.game.constant.CommandConstants;

/**
 *
 * @author Stamos
 */
public abstract class AbstractCommand {
    
    protected static final String EXIT_COMMAND =
            CommandConstants.EXIT.name().toLowerCase();
    
    protected static final String SAVE_COMMAND =
            CommandConstants.SAVE.name().toLowerCase();
    
    protected static final String LOAD_COMMAND =
            CommandConstants.LOAD.name().toLowerCase();
    
    protected static final String GO_COMMAND =
            CommandConstants.GO.name().toLowerCase();
    
    protected static final String RUN_COMMAND =
            CommandConstants.RUN.name().toLowerCase();
    
    protected static final String LOOK_COMMAND =
            CommandConstants.LOOK.name().toLowerCase();
    
    protected static final String TAKE_COMMAND =
            CommandConstants.TAKE.name().toLowerCase();
    
    protected static final String DROP_COMMAND =
            CommandConstants.DROP.name().toLowerCase();
    
    protected static final String OPEN_COMMAND =
            CommandConstants.OPEN.name().toLowerCase();
    
    protected static final String CLOSE_COMMAND =
            CommandConstants.CLOSE.name().toLowerCase();
    
    protected static final String SHOOT_COMMAND =
            CommandConstants.SHOOT.name().toLowerCase();
    
    protected static final String NO_OBJECTIVE = "";
    
    protected String name;
    
    protected String objective;
    
    protected AbstractGame game;
    
    /**
     * Initializes a newly created <tt>AbstractCommand</tt> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     * @param objective
     */
    protected AbstractCommand(String name, String objective) {
        this.name = name;
        
        this.objective = objective;
        
        game = null;
    }
    
    public String getName() {
        return name;
    }
    
    public String getObjective() {
        return objective;
    }
    
    public AbstractGame getGame() {
        return game;
    }
    
    public void setGame(AbstractGame game) {
        this.game = game;
    }
    
    public boolean isValid() {
        return false;
    }
    
    public abstract void executeCommand();
    
}
