/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.ShootAction;

/**
 *
 * @author Stamos
 */
public class ShootCommand extends AbstractCommand {
    
    public ShootCommand(String objective) {
        super(SHOOT_COMMAND, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        ShootAction action = new ShootAction(objective);
        
        game.setAction(action);
    }
    
}
