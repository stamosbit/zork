/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.TakeAction;

/**
 *
 * @author Stamos
 */
public class TakeCommand extends AbstractCommand {
    
    public TakeCommand(String objective) {
        super(TAKE_COMMAND, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        TakeAction action = new TakeAction(objective);
        
        game.setAction(action);
    }
    
}
