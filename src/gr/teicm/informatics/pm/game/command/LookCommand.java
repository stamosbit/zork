/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.action.LookAction;

/**
 *
 * @author Stamos
 */
public class LookCommand extends AbstractCommand {
    
    public LookCommand(String objective) {
        super(LOOK_COMMAND, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        AbstractAction action = new LookAction();
        
        game.setAction(action);
    }
    
}
