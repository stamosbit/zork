/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.constant.CommandConstants;

/**
 *
 * @author Stamos
 */
public abstract class AbstractGame {
    
    protected final String title;
    
    protected AbstractAction action;
    
    protected String outcome;
    
    /**
     * Initializes a newly created <tt>AbstractGame</tt> object. Use this
     * constructor from subclasses.
     * 
     * @param title
     */
    protected AbstractGame(String title) {
        this.title = title;
        
        action = null;
        
        outcome = "";
    }
    
    public void setAction(AbstractAction action) {
        this.action = action;
    }
    
    public String getOutcome() {
        return outcome;
    }
    
    public void run() {
        if (!(action == null)) {
            if (!(CommandConstants.EXIT.name().equalsIgnoreCase(action.getName()))) {
                start();
            } else {
                finish();
            }
        }
    }
    
    protected abstract void start();
    
    protected abstract void finish();
    
}
