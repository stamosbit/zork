/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.controller;

import gr.teicm.informatics.pm.game.constant.DirectionConstants;
import gr.teicm.informatics.pm.game.model.container.ISceneContainer;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.scene.Scene;

/**
 *
 * @author Stamos
 */
class MoveActorController implements java.io.Serializable {
    
    private static final DirectionConstants NORTH = DirectionConstants.NORTH;
    
    private static final DirectionConstants SOUTH = DirectionConstants.SOUTH;
    
    private static final DirectionConstants WEST = DirectionConstants.WEST;
    
    private static final DirectionConstants EAST = DirectionConstants.EAST;
    
    private transient AbstractActor actor;
    
    private transient ISceneContainer sceneContainer;
    
    /**
     * Initializes a newly created <tt>Context</tt> object.
     */
    public MoveActorController() {
        reset();
    }
    
    /**
     *
     * @return
     */
    public AbstractActor getActor() {
        return actor;
    }
    
    /**
     *
     * @param actor
     */
    public void setActor(AbstractActor actor) {
        this.actor = actor;
    }
    
    /**
     *
     * @return
     */
    public ISceneContainer getSceneMap() {
        return sceneContainer;
    }
    
    /**
     *
     * @param sceneMap
     */
    public void setSceneMap(ISceneContainer sceneMap) {
        this.sceneContainer = sceneMap;
    }
    
    /**
     *
     */
    public final void reset() {
        actor = null;
        
        sceneContainer = null;
    }
    
    /**
     *
     * @param direction
     */
    public void moveActor(DirectionConstants direction) {
        String currentSceneName = actor.getCurrentSceneName();
        
        Scene currentScene = sceneContainer.getScene(currentSceneName);
        
        if (NORTH.equals(direction)) {
            String northSceneName = currentScene.getNorthSceneName();
            
            if (!(northSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(northSceneName);
                
                Scene northScene = sceneContainer.getScene(northSceneName);
                
                northScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(northScene);
            }
        } else if (SOUTH.equals(direction)) {
            String southSceneName = currentScene.getSouthSceneName();
            
            if (!(southSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(southSceneName);
                
                Scene southScene = sceneContainer.getScene(southSceneName);
                
                southScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(southScene);
            }
        } else if (WEST.equals(direction)) {
            String westSceneName = currentScene.getWestSceneName();
            
            if (!(westSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(westSceneName);
                
                Scene westScene = sceneContainer.getScene(westSceneName);
                
                westScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(westScene);
            }
        } else if (EAST.equals(direction)) {
            String eastSceneName = currentScene.getEastSceneName();
            
            if (!(eastSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(eastSceneName);
                
                Scene eastScene = sceneContainer.getScene(eastSceneName);
                
                eastScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(eastScene);
            }
        }
    }
    
}
