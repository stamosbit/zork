/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.factory;

/**
 *
 * @author Stamos
 * @param <T>
 */
interface IFactory<T> {
    
    T getInstance(String type) throws IllegalArgumentException;
    
}
