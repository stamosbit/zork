/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.factory;

import gr.teicm.informatics.pm.game.model.item.consumable.Drug;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.item.consumable.Potion;
import gr.teicm.informatics.pm.game.model.item.miscellaneous.Book;
import gr.teicm.informatics.pm.game.model.item.miscellaneous.Chest;
import gr.teicm.informatics.pm.game.model.item.miscellaneous.Door;
import gr.teicm.informatics.pm.game.model.item.weapon.Knife;
import gr.teicm.informatics.pm.game.model.item.weapon.Pistol;

/**
 *
 * @author Stamos
 */
public class ItemFactory extends Factory<AbstractItem> {
    
    public ItemFactory() {
        super("Item");
        
        initConsumables();
        
        initMiscellaneous();
        
        initWeapons();
    }
    
    private void initConsumables() {
        // init objects.
        Potion potion = new Potion();
        
        Drug drug = new Drug();
        
        // prepare keys.
        String potionName = potion.getName();
        
        String drugName = drug.getName();
        
        // put keys and objects.
        hashMap.put(potionName, potion);
        
        hashMap.put(drugName, drug);
    }
    
    private void initMiscellaneous() {
        // init objects.
        Book book = new Book();
        
        Door door = new Door();
        
        Chest chest = new Chest();
        
        // prepare keys.
        String bookName = book.getName();
        
        String doorName = door.getName();
        
        String chestName = chest.getName();
        
        // put keys and objects.
        hashMap.put(bookName, book);
        
        hashMap.put(doorName, door);
        
        hashMap.put(chestName, chest);
    }
    
    private void initWeapons() {
        // init objects.
        Knife knife = new Knife();
        
        Pistol pistol = new Pistol();
        
        // prepare keys.
        String knifeName = knife.getName();
        
        String pistolName = pistol.getName();
        
        // put keys and objects.
        hashMap.put(knifeName, knife);
        
        hashMap.put(pistolName, pistol);
    }
    
}
