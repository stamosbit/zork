/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.factory;

import java.util.HashMap;

/**
 *
 * @author Stamos
 * @param <T>
 */
class Factory<T> implements IFactory<T> {
    
    protected final String factoryType;
    
    protected HashMap<String,T> hashMap;
    
    protected Factory(String factoryType) {
        this.factoryType = factoryType;
        
        hashMap = new HashMap<String,T>();
    }
    
    @Override
    public T getInstance(String type) throws IllegalArgumentException {
        String key = type.toLowerCase();
        
        if (hashMap.containsKey(key)) {
            T instance = hashMap.get(key);
            
            return instance;
        } else {
            IllegalArgumentException exception = new IllegalArgumentException(
                    "Invalid " + factoryType + "..."
            );
            
            throw exception;
        }
    }
    
}
