/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.factory;

import gr.teicm.informatics.pm.game.command.AbstractCommand;
import gr.teicm.informatics.pm.game.command.CloseCommand;
import gr.teicm.informatics.pm.game.command.ExitCommand;
import gr.teicm.informatics.pm.game.command.GoCommand;
import gr.teicm.informatics.pm.game.command.LoadCommand;
import gr.teicm.informatics.pm.game.command.OpenCommand;
import gr.teicm.informatics.pm.game.command.RunCommand;
import gr.teicm.informatics.pm.game.command.SaveCommand;
import gr.teicm.informatics.pm.game.command.ShootCommand;
import gr.teicm.informatics.pm.game.command.TakeCommand;

/**
 *
 * @author Stamos
 */
public class CommandFactory extends Factory<AbstractCommand> {
    
    /**
     * Initializes a newly created <tt>CommandFactory</tt> object.
     * 
     * @param objective
     */
    public CommandFactory(String objective) {
        super("Command");
        
        // Put in the collection below any class' instance that extends
        // AbstractCommand.
        hashMap.put("exit", new ExitCommand());
        
        hashMap.put("save", new SaveCommand());
        
        hashMap.put("load", new LoadCommand());
        
        hashMap.put("go", new GoCommand(objective));
        
        hashMap.put("run", new RunCommand(objective));
        
        hashMap.put("take", new TakeCommand(objective));
        
        hashMap.put("open", new OpenCommand(objective));
        
        hashMap.put("close", new CloseCommand(objective));
        
        hashMap.put("shoot", new ShootCommand(objective));
    }
    
}
