/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

import gr.teicm.informatics.pm.game.constant.CommandConstants;

import java.util.List;

/**
 *
 * @author Stamos
 */
class CommandFinder extends AbstractFinder {
    
    public CommandFinder(List<String> words) {
        super(words);
    }
    
    @Override
    public void searchTarget() {
        for (CommandConstants command : CommandConstants.values()) {
            for (String word : words) {
                if (command.name().equalsIgnoreCase(word)) {
                    target = word.toLowerCase();
                    targetFound = true;
                }
            }
        }
        
        if (target.equals("")) {
            targetFound = false;
        }
    }
    
}
