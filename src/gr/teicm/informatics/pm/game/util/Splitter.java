/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author Stamos
 */
class Splitter {
    
    StringTokenizer tokenizer;
    
    private final List<String> words = new ArrayList<>();
    
    public Splitter(Scanner scanner) {
        tokenizer = new StringTokenizer(scanner.nextLine());
        
        splitInputToWords();
    }
    
    public List<String> getSplitWords() {
        return words;
    }
    
    private void splitInputToWords() {
        while (tokenizer.hasMoreTokens()) {
            words.add(tokenizer.nextToken());
        }
    }
    
}
