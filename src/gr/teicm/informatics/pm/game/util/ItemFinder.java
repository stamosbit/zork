/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

import gr.teicm.informatics.pm.game.constant.ItemConstants;

import java.util.List;

/**
 *
 * @author Stamos
 */
class ItemFinder extends AbstractFinder {
    
    public ItemFinder(List<String> words) {
        super(words);
    }
    
    @Override
    public void searchTarget() {
        for (ItemConstants item : ItemConstants.values()) {
            for (String word : words) {
                if (item.name().equalsIgnoreCase(word)) {
                    target = word.toLowerCase();
                    targetFound = true;
                }
            }
        }
        
        if (target.equals("")) {
            targetFound = false;
        }
    }
    
}
