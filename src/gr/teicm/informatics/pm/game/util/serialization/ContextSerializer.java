/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util.serialization;

import gr.teicm.informatics.pm.game.controller.Context;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;

import javax.swing.JOptionPane;

/**
 *
 * @author Stamos
 */
public class ContextSerializer implements ISerializer {
    
    private static final String SAVE_SUCCESS = "Game Saved!";
    
    private final String pathName;
    
    private final JOptionPane successPane;
    
    private final JOptionPane failurePane;
    
    private final Context context;
    
    public ContextSerializer(Context context) {
        pathName =
                "C:\\Users\\4340\\Documents\\Bitbucket\\zork\\save\\context.ser";
        
        successPane = new JOptionPane(
                SAVE_SUCCESS, JOptionPane.INFORMATION_MESSAGE
        );
        
        successPane.setVisible(true);
        
        failurePane = new JOptionPane(JOptionPane.ERROR_MESSAGE);
        
        failurePane.setVisible(true);
        
        this.context = context;
    }
    
    @Override
    public void serialize() {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream(pathName);
            
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            
            objectOut.writeObject(context);
            
            objectOut.close();
            
            fileOut.close();
        } catch (IOException ex) {
            //failurePane.setMessage(ex.getMessage());
            
            System.out.println(ex.getMessage());
        }
    }
    
}
