/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util.serialization;

import gr.teicm.informatics.pm.game.controller.Context;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author Stamos
 */
public class ContextDeserializer implements IDeserializer<Context> {
    
    private final String pathName =
            "C:\\Users\\4340\\Documents\\Bitbucket\\zork\\save\\context.ser";
    
    private Context context;
    
    public ContextDeserializer() {
        context = null;
    }
    
    @Override
    public void deserialize() {
        try {
            FileInputStream fileIn = new FileInputStream(pathName);
            
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            
            context = (Context) objectIn.readObject();
            
            objectIn.close();
            
            fileIn.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public Context getInstance() {
        return context;
    }
    
    
    
}
