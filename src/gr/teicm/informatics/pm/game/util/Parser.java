/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

import gr.teicm.informatics.pm.game.factory.CommandFactory;
import gr.teicm.informatics.pm.game.command.AbstractCommand;

import java.util.Scanner;
import java.util.List;

/**
 *
 * @author Stamos
 */
public class Parser {
    
    private final Splitter splitter;
    
    private final CommandFinder commandFinder;
    private final DirectionFinder directionFinder;
    private final ItemFinder itemFinder;
    private final ActorFinder actorFinder;
    
    private String commandString;    
    private String objectiveString;
    
    public Parser(Scanner scanner) {
        splitter = new Splitter(scanner);
        
        List<String> splitWords = splitter.getSplitWords();
        
        // initFinders
        commandFinder     = new     CommandFinder(splitWords);
        directionFinder   = new   DirectionFinder(splitWords);
        itemFinder        = new        ItemFinder(splitWords);
        actorFinder       = new       ActorFinder(splitWords);
        
        commandString = "";
        objectiveString = "";
        
        parseInput();
    }
    
    public AbstractCommand getCommand() throws IllegalArgumentException {
        CommandFactory commandFactory = new CommandFactory(objectiveString);
                
        return commandFactory.getInstance(commandString);
    }
    
    private void parseInput() {
        findCommand();
        
        findObjective();
    }
    
    private void findCommand() {
        commandFinder.searchTarget();
        if (commandFinder.targetIsFound()) {
            commandString = commandFinder.getTarget();
        }
    }
    
    private void findObjective() {
        switch (commandString) {
            case "go":
                directionFinder.searchTarget();
                if (directionFinder.targetIsFound()) {
                    objectiveString = directionFinder.getTarget();
                }
                break;
            
            case "take":
                itemFinder.searchTarget();
                if (itemFinder.targetIsFound()) {
                    objectiveString = itemFinder.getTarget();
                }
                break;
            
            case "run":
                directionFinder.searchTarget();
                if (directionFinder.targetIsFound()) {
                    objectiveString = directionFinder.getTarget();
                }
                break;
            
            case "open":
                itemFinder.searchTarget();
                if (itemFinder.targetIsFound()) {
                    objectiveString = itemFinder.getTarget();
                }
                break;
            
            case "close":
                itemFinder.searchTarget();
                if (itemFinder.targetIsFound()) {
                    objectiveString = itemFinder.getTarget();
                }
                break;
            
            case "shoot":
                actorFinder.searchTarget();
                if (actorFinder.targetIsFound()) {
                    objectiveString = actorFinder.getTarget();
                }
                break;
            
            default:
                objectiveString = "";
                break;
        }
    }
    
}
