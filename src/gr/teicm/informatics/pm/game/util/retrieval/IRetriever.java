/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util.retrieval;

/**
 *
 * @author Stamos
 * @param <T>
 */
public interface IRetriever<T> {
    
    void retrieve();
    
    T getInstance();
    
}
