/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.container;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public interface IActorContainer extends java.io.Serializable {
    
    /**
     * Returns the collection of <tt>AbstractActor</tt> objects.
     * 
     * @return the collection of AbstractActor objects
     */
    HashMap<String,AbstractActor> getAllActors();
    
    /**
     * Returns an <tt>AbstractActor</tt> object if the collection contains an entry
     * keyed with <tt>key</tt>.
     * 
     * @param key  the name of the preferable AbstractActor object
     * @return an AbstractActor object
     */
    AbstractActor getActor(String key);
    
    /**
     * Puts <tt>actor</tt> into the collection if an entry like that is
     * <i>not</i> already contained inside collection.
     * 
     * @param actor  the AbstractActor object to be put into the collection
     */
    void putActor(AbstractActor actor);
    
    /**
     * Updates <tt>actor</tt> in the collection if an entry like that is already
     * contained inside the collection.
     * 
     * @param actor  the AbstractActor object to be updated in the colletion
     */
    void updateActor(AbstractActor actor);
    
    /**
     * Deletes <tt>actor</tt> from the collection if an entry like that is
     * contained inside the collection.
     * 
     * @param actor  the AbstractActor object to be deleted from the collection
     */
    void deleteActor(AbstractActor actor);
    
}
