/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.container;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public interface IItemContainer extends java.io.Serializable {
    
    /**
     * Returns the collection of <tt>AbstractItem</tt> objects.
     * 
     * @return the collection of AbstractItem objects
     */
    HashMap<String,AbstractItem> getAllItems();
    
    /**
     * Returns an <tt>AbstractItem</tt> object if the collection contains an entry named
     * with <tt>key</tt>.
     * 
     * @param key  the name of the preferable AbstractItem object
     * @return item object
     */
    AbstractItem getItem(String key);
    
    /**
     * Puts <tt>item</tt> into the collection if an entry like that is
     * <i>not</i> already contained inside the collection.
     * 
     * @param item  the AbstractItem object to be put into the collection
     */
    void putItem(AbstractItem item);
    
    /**
     * Updates <tt>item</tt> in the collection if an entry like that is already
     * contained inside the collection.
     * 
     * @param item  the AbstractItem object to be updated in the collection
     */
    void updateItem(AbstractItem item);
    
    /**
     * Deletes <tt>item</tt> from the collection if an entry like that is
     * already contained inside the collection.
     * 
     * @param item  the AbstractItem object to be deleted from the collection
     */
    void deleteItem(AbstractItem item);
    
}
