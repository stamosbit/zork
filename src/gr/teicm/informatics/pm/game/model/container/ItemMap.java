/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.container;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public class ItemMap implements IItemContainer {

    /**
     * The collection of <tt>AbstractItem</tt> objects.
     */
    private final HashMap<String,AbstractItem> itemHashMap;
    
    /**
     * Initializes a newly created <tt>ItemMap</tt> object.
     */
    public ItemMap() {
        itemHashMap = new HashMap<String,AbstractItem>();
    }
    
    @Override
    public HashMap<String,AbstractItem> getAllItems() {
        return itemHashMap;
    }
    
    @Override
    public AbstractItem getItem(String key) {
        if (itemHashMap.containsKey(key)) {
            return itemHashMap.get(key);
        }
        
        return null;
    }
    
    @Override
    public void putItem(AbstractItem item) {
        String key = item.getName();
        
        if (!itemHashMap.containsKey(key)) {
            itemHashMap.put(key, item);
        }
    }
    
    @Override
    public void updateItem(AbstractItem item) {
        String key = item.getName();
        
        if (itemHashMap.containsKey(key)) {
            itemHashMap.replace(key, item);
        }
    }
    
    @Override
    public void deleteItem(AbstractItem item) {
        String key = item.getName();
        
        if (itemHashMap.containsKey(key)) {
            itemHashMap.remove(key, item);
        }
    }
    
}
