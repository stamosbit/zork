/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.container;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public class ActorMap implements IActorContainer {
    
    /**
     * The collection of <tt>AbstractActor</tt> objects.
     */
    private final HashMap<String,AbstractActor> actorHashMap;
    
    /**
     * Initializes a newly created <tt>ActorMap</tt> object.
     */
    public ActorMap() {
        actorHashMap = new HashMap<String,AbstractActor>();
    }
    
    @Override
    public HashMap<String,AbstractActor> getAllActors() {
        return actorHashMap;
    }
    
    @Override
    public AbstractActor getActor(String key) {
        if (actorHashMap.containsKey(key)) {
            return actorHashMap.get(key);
        }
        
        return null;
    }
    
    @Override
    public void putActor(AbstractActor actor) {
        String key = actor.getName();
        
        if (!actorHashMap.containsKey(key)) {
            actorHashMap.put(key, actor);
        }
    }
    
    @Override
    public void updateActor(AbstractActor actor) {
        String key = actor.getName();
        
        if (actorHashMap.containsKey(key)) {
            actorHashMap.replace(key, actor);
        }
    }
    
    @Override
    public void deleteActor(AbstractActor actor) {
        String key = actor.getName();
        
        if (actorHashMap.containsKey(key)) {
            actorHashMap.remove(key, actor);
        }
    }
    
}
