/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.container;

import gr.teicm.informatics.pm.game.model.scene.Scene;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public interface ISceneContainer extends java.io.Serializable {
    
    /**
     * Returns the collection of <tt>Scene</tt> objects.
     * 
     * @return the collection of Scene objects
     */
    HashMap<String,Scene> getAllScenes();
    
    /**
     * Returns an <tt>Scene</tt> object if the collection contains an entry named
     * with <tt>key</tt>.
     * 
     * @param key  the name of the preferable Scene object
     * @return Scene object
     */
    Scene getScene(String key);
    
    /**
     * Puts <tt>scene</tt> into the collection if an entry like that is
     * <i>not</i> already contained inside the collection.
     * 
     * @param scene  the Scene object to be put into the collection
     */
    void putScene(Scene scene);
    
    /**
     * Updates <tt>scene</tt> in the collection if an entry like that is already
     * contained inside the collection.
     * 
     * @param scene  the Scene object to be updated in the collection
     */
    void updateScene(Scene scene);
    
    /**
     * Deletes <tt>scene</tt> from the collection if an entry like that is
     * already contained inside the collection.
     * 
     * @param scene  the Scene object to be deleted from the collection
     */
    void deleteScene(Scene scene);
    
}
