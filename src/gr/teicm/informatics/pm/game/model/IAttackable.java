/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model;

/**
 *
 * @author Stamos
 */
public interface IAttackable extends java.io.Serializable {
    
    /**
     * Returns the amount of damage this object can do.
     * 
     * @return
     */
    Integer attack();
    
}
