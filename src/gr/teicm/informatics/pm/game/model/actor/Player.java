/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor;

import gr.teicm.informatics.pm.game.model.ITakeable;
import gr.teicm.informatics.pm.game.model.IOpenable;
import gr.teicm.informatics.pm.game.model.IOpening;
import gr.teicm.informatics.pm.game.model.IShootable;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;

/**
 *
 * @author Stamos
 */
public class Player extends AbstractActor
implements IOpenable, ITakeable, IShootable {
    
    /**
     * Initializes a newly created <tt>Player</tt> object.
     */
    public Player() {
        super(PLAYER_NAME, "You are this hero!");
        
        healthPoints = 100;
    }
    
    /**
     * Makes <tt>openingItem</tt> opened and returns it.
     * 
     * @param openingItem
     * @return the openingItem opened
     */
    @Override
    public IOpening open(IOpening openingItem) {
        openingItem.makeOpened();
        
        return openingItem;
    }
    
    /**
     * Makes <tt>openingItem</tt> closed and returns it.
     * 
     * @param openingItem
     * @return the openingItem closed
     */
    @Override
    public IOpening close(IOpening openingItem) {
        openingItem.makeClosed();
        
        return openingItem;
    }
    
    /**
     * Adds <tt>item</tt> to <tt>inventory</tt>.
     * 
     * @param item
     */
    @Override
    public void take(AbstractItem item) {
        putItem(item);
    }
    
    @Override
    public Integer stab() {
        Integer damage = knife.getDamageDone();
        
        return damage;
    }
    
    @Override
    public Integer shoot() {
        Integer damage = pistol.getDamageDone();
        
        return damage;
    }
    
}
