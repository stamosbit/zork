/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor;

import gr.teicm.informatics.pm.game.model.GameComponent;
import gr.teicm.informatics.pm.game.model.container.IItemContainer;
import gr.teicm.informatics.pm.game.model.IShootable;
import gr.teicm.informatics.pm.game.model.IStabable;
import gr.teicm.informatics.pm.game.model.Inventory;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.item.weapon.AbstractWeapon;
import gr.teicm.informatics.pm.game.constant.ActorConstants;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public abstract class AbstractActor extends GameComponent
implements IItemContainer, IStabable, IShootable {
    
    protected static final String PLAYER_NAME =
            ActorConstants.PLAYER.name().toLowerCase();
    
    protected static final String PIRATE_NAME =
            ActorConstants.PIRATE.name().toLowerCase();
    
    /**
     * Indicates the health that the actor has.
     */
    protected Integer healthPoints;
    
    /**
     * Indicates the name of the <tt>Scene</tt> object that the actor takes place.
     */
    protected String currentSceneName;
    
    /**
     * The <tt>AbstractItem</tt> objects container.
     */
    protected IItemContainer inventory;
    
    /**
     * A <tt>Knife</tt> object.
     */
    protected AbstractWeapon knife;
    
    /**
     * A <tt>Pistol</tt> object
     */
    protected AbstractWeapon pistol;
    
    /**
     * Initializes a newly created <tt>AbstractItem</tt> object. Use this constructor
     * from subclasses.
     * 
     * @param name
     * @param description
     */
    protected AbstractActor(String name, String description) {
        super(name, description);
        
        healthPoints = 0;
        
        inventory = new Inventory();
    }
    
    /**
     * Returns <tt>healthPoints</tt>.
     * 
     * @return healthPoints
     */
    public Integer getHealthPoints() {
        return healthPoints;
    }
    
    /**
     * Sets healthPoints to new value if the new value is >= 0.
     * 
     * @param healthPoints  must be >= 0
     */
    public void setHealthPoints(Integer healthPoints) {
        if (healthPoints >= 0) {
            this.healthPoints = healthPoints;
        }
    }
    
    /**
     * Returns <tt>currentSceneName</tt>.
     * 
     * @return currentSceneName
     */
    public String getCurrentSceneName() {
        return currentSceneName;
    }
    
    /**
     * Sets <tt>currentSceneName</tt> to new value.
     * 
     * @param currentSceneName
     */
    public void setCurrentSceneName(String currentSceneName) {
        if (currentSceneName == null) {
            this.currentSceneName = "";
        }
        this.currentSceneName = currentSceneName;
    }

    @Override
    public HashMap<String,AbstractItem> getAllItems() {
        HashMap<String,AbstractItem> itemHashMap = inventory.getAllItems();
        
        return itemHashMap;
    }

    @Override
    public AbstractItem getItem(String key) {
        AbstractItem item = inventory.getItem(key);
        
        return item;
    }

    @Override
    public void putItem(AbstractItem item) {
        inventory.putItem(item);
    }

    @Override
    public void updateItem(AbstractItem item) {
        inventory.updateItem(item);
    }

    @Override
    public void deleteItem(AbstractItem item) {
        inventory.deleteItem(item);
    }
    
    public void setKnife(AbstractWeapon knife) {
        this.knife = knife;
    }
    
    public void setPistol(AbstractWeapon pistol) {
        this.pistol = pistol;
    }
    
}
