/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model;

/**
 *
 * @author Stamos
 */
public class GameComponent implements java.io.Serializable {
    
    /**
     * Indicates the object's name.
     */
    protected String name;
    
    /**
     * Describes the object.
     */
    protected String description;
    
    /**
     * Initializes a newly created <tt>Entity</tt> object. Use this constructor
     * from subclasses.
     * 
     * @param name
     * @param description
     */
    protected GameComponent(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
    /**
     * Returns object's <tt>name</tt>.
     * 
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * Sets a newly value to <tt>name</tt>.
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Returns the object's description.
     * 
     * @return description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * Sets a newly value to <tt>description</tt>.
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
}
