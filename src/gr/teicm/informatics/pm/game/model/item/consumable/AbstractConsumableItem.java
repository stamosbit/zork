/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.consumable;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;

/**
 *
 * @author Stamos
 */
public abstract class AbstractConsumableItem extends AbstractItem<Integer> {
    
    Integer amountOfEffect;
    
    protected AbstractConsumableItem(String name, String description) {
        super(name, description);
        
        amountOfEffect = 0;
    }
    
    public Integer getAmountOfEffect() {
        return amountOfEffect;
    }
    
    @Override
    public Integer use() {
        return getAmountOfEffect();
    }
    
}
