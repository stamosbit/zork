/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.weapon;

/**
 *
 * @author Stamos
 */
public class Pistol extends AbstractWeapon {
    
    /**
     * Initializes a newly created <tt>Gun</tt> object.
     */
    public Pistol() {
        super("pistol", "This is a gun. You can shoot someone with it.");
        
        damageDone = 10;
    }
    
    @Override
    public Boolean isTakable() {
        return true;
    }

    @Override
    public Integer attack() {
        return damageDone;
    }
    
}
