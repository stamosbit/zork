/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.miscellaneous;

/**
 *
 * @author Stamos
 */
public class Book extends AbstractMiscellaneous<String> {
    
    protected String context;

    public Book() {
        super(BOOK_NAME, "This is a book. You can read it.");
    }
    
    @Override
    public Boolean isTakable() {
        return true;
    }
    
    public String getContext() {
        return context;
    }
    
    @Override
    public String use() {
        return getContext();
    }
    
}
