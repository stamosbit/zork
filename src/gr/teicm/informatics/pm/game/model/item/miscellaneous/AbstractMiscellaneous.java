/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.miscellaneous;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;

/**
 *
 * @author Stamos
 * @param <T>
 */
public abstract class AbstractMiscellaneous<T> extends AbstractItem<T> {
    
    /**
     * Initializes a newly created <tt>Miscellaneous</tt> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     * @param description
     */
    protected AbstractMiscellaneous(String name, String description) {
        super(name, description);
    }
    
}
