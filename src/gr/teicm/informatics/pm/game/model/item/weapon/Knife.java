/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.weapon;

/**
 *
 * @author Stamos
 */
public class Knife extends AbstractWeapon {
    
    /**
     * Initializes a newly created <tt>Knife</tt> object.
     */
    public Knife() {
        super("knife", "This is a knife. You can stab someone with it.");
        
        damageDone = 5;
    }
    
    @Override
    public Boolean isTakable() {
        return true;
    }
    
    @Override
    public Integer attack() {
        return damageDone;
    }
    
}
