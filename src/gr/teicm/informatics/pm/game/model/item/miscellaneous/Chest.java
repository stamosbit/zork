/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.miscellaneous;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.container.IItemContainer;
import gr.teicm.informatics.pm.game.model.IOpening;
import gr.teicm.informatics.pm.game.model.container.ItemMap;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public class Chest extends AbstractMiscellaneous<Boolean> implements IOpening, IItemContainer {
    
    /**
     * Indicates if the chest is opened for access.
     */
    private Boolean opened;
    
    /**
     * Stores all the <tt>item</tt> objects that will be contained.
     */
    private final ItemMap itemMap;
    
    /**
     * Initializes a newly created <tt>Chest</tt> object.
     */
    public Chest() {
        super(CHEST_NAME, "This is a chest. It can contain items.");
        
        opened = false;
        
        itemMap = new ItemMap();
    }
    
    @Override
    public Boolean isTakable() {
        return false;
    }
    
    @Override
    public Boolean isOpened() {
        return opened;
    }
    
    @Override
    public void makeOpened() {
        opened = true;
    }
    
    @Override
    public void makeClosed() {
        opened = false;
    }
    
    @Override
    public HashMap<String,AbstractItem> getAllItems() {
        HashMap<String,AbstractItem> itemHashMap = itemMap.getAllItems();
        
        return itemHashMap;
    }
    
    @Override
    public AbstractItem getItem(String key) {
        AbstractItem item = itemMap.getItem(key);
        
        return item;
    }
    
    @Override
    public void putItem(AbstractItem item) {
        itemMap.putItem(item);
    }
    
    @Override
    public void updateItem(AbstractItem item) {
        itemMap.updateItem(item);
    }
    
    @Override
    public void deleteItem(AbstractItem item) {
        itemMap.deleteItem(item);
    }
    
    @Override
    public Boolean use() {
        if (!isOpened()) {
            makeOpened();
        } else {
            makeClosed();
        }
        
        return isOpened();
    }
    
}
