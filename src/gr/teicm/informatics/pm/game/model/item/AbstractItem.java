/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item;

import gr.teicm.informatics.pm.game.constant.ItemConstants;
import gr.teicm.informatics.pm.game.model.GameComponent;

/**
 *
 * @author Stamos
 * @param <T>
 */
public abstract class AbstractItem<T> extends GameComponent {
    
    protected static final String POTION_NAME =
            ItemConstants.POTION.name().toLowerCase();
    
    protected static final String DRUG_NAME =
            ItemConstants.DRUG.name().toLowerCase();
    
    protected static final String KNIFE_NAME =
            ItemConstants.KNIFE.name().toLowerCase();
    
    protected static final String PISTOL_NAME =
            ItemConstants.PISTOL.name().toLowerCase();
    
    protected static final String BOOK_NAME =
            ItemConstants.BOOK.name().toLowerCase();
    
    protected static final String CHEST_NAME =
            ItemConstants.CHEST.name().toLowerCase();
    
    protected static final String DOOR_NAME =
            ItemConstants.DOOR.name().toLowerCase();
    
    /**
     * Holds the name of the <tt>Actor</tt> object which is contained into.
     */
    protected String carrierActorName;
    
    /**
     * Initializes a newly created <tt>Item</tt> object. Use this constructor
     * from subclasses.
     * 
     * @param name
     * @param description
     */
    protected AbstractItem(String name, String description) {
        super(name, description);
        
        carrierActorName = "";
    }
    
    /**
     * Returns <tt>true</tt> if this <tt>AbstractItem</tt> object is takable.
     * 
     * @return a boolean value
     */
    public abstract Boolean isTakable();
    
    /**
     * Returns a value depending on the <tt>AbstractItem</tt> object that is
     * used.
     * 
     * @return a value depending on the AbstractItem object that is used
     */
    public abstract T use();
    
    /**
     * Returns the <tt>Actor</tt> object's <tt>name</tt> which indicates who
     * carries the item.
     * 
     * @return Actor object's name
     */
    public String getCarrierActorName() {
        return carrierActorName;
    }
    
    /**
     * Sets a new value to the <tt>Actor</tt> object's name which indicates who
     * carries the item.
     * 
     * @param carrierActorName
     */
    public void setCarrierActorName(String carrierActorName) {
        this.carrierActorName = carrierActorName;
    }
    
}
