/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 *
 * @author Stamos
 */
public class Farm extends Scene {
    
    /**
     * Initializes a newly created <tt>Farm</tt> object.
     */
    public Farm() {
        super(
                FARM_NAME,
                "This is a farm. You can milk cows for potions."
        );
    }
    
}
