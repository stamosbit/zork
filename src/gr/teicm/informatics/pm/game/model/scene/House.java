/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 *
 * @author Stamos
 */
public class House extends Scene {
    
    /**
     * Initializes a newly created <tt>House</tt> object.
     */
    public House() {
        super(
                HOUSE_NAME,
                "This is your house. You can try going to a direction."
        );
    }
    
}
