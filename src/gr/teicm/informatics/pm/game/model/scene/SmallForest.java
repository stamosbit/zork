/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 *
 * @author Stamos
 */
public class SmallForest extends Scene  {
    
    /**
     * Initializes a newly created <tt>SmallForest</tt> object.
     */
    public SmallForest() {
        super(
                SMALL_FOREST_NAME,
                "This is a small forest. You can hear the birds tweeting."
        );
    }
    
}
