/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 *
 * @author Stamos
 */
public class BigForest extends Scene {
    
    /**
     * Initializes a newly created <tt>BigForest</tt> object.
     */
    public BigForest() {
        super(
                BIG_FOREST_NAME,
                "This is a big forest. You can hear wild animals roaring! Be ware!"
        );
    }
    
}
