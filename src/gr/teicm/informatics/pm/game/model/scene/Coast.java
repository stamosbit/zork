/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 *
 * @author Stamos
 */
public class Coast extends Scene {
    
    public Coast() {
        super(
                COAST_NAME,
                "This is the coast. It looks like the pirate arrived here."
        );
    }
    
}
