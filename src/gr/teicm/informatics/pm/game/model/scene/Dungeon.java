/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 *
 * @author Stamos
 */
public class Dungeon extends Scene {
    
    /**
     * Initializes a newly created <tt>Dungeon</tt> object.
     */
    public Dungeon() {
        super(
                DUNGEON_NAME,
                "This is a Dungeon. Be ware!"
        );
    }
    
}
