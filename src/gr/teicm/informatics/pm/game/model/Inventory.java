/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model;

import gr.teicm.informatics.pm.game.model.container.IItemContainer;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.container.ItemMap;

import java.util.HashMap;

/**
 *
 * @author Stamos
 */
public class Inventory extends GameComponent implements IItemContainer {
    
    /**
     * Stores all the <tt>item</tt> objects that will be contained.
     */
    private final IItemContainer itemMap;
    
    /**
     * Initializes a newly created <tt>Inventory</tt> object.
     */
    public Inventory() {
        super("Inventory", "This is an inventory. It can contain items.");
        
        itemMap = new ItemMap();
    }
    
    @Override
    public HashMap<String,AbstractItem> getAllItems() {
        HashMap<String,AbstractItem> itemHashMap = itemMap.getAllItems();
        
        return itemHashMap;
    }
    
    @Override
    public AbstractItem getItem(String key) {
        AbstractItem item = itemMap.getItem(key);
        
        return item;
    }
    
    @Override
    public void putItem(AbstractItem item) {
        itemMap.putItem(item);
    }
    
    @Override
    public void updateItem(AbstractItem item) {
        itemMap.updateItem(item);
    }
    
    @Override
    public void deleteItem(AbstractItem item) {
        itemMap.deleteItem(item);
    }
    
}
