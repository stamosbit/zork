/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game;

import gr.teicm.informatics.pm.game.controller.Context;

/**
 *
 * @author Stamos
 */
public class TextBasedAdventureGame extends AbstractGame {
    
    private Context context;
    
    /**
     * Initializes a newly created <tt>TextBasedAdventureGame</tt> object.
     * 
     * @param title
     */
    public TextBasedAdventureGame(String title) {
        super(title);
        
        context = new Context();
    }
    
    @Override
    protected void start() {
        action.setContext(context);
        
        action.performAction();
        
        context = action.getContext();
        
        outcome = context.getCurrentOutcome();
        
        action = null;
    }
    
    @Override
    protected void finish() {
        action.performAction();
    }
    
}
