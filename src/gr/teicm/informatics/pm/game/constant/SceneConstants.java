/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.constant;

/**
 *
 * @author Stamos
 */
public enum SceneConstants implements java.io.Serializable {
    
    HOUSE, FARM, BIG_FOREST, SMALL_FOREST, DUNGEON, COAST
    
}
